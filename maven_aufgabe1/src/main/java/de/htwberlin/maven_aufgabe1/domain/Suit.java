package de.htwberlin.maven_aufgabe1.domain;
//
/**
 * Enumerations for the suit of the cards
 */
public enum Suit {

	spades, hearts, diamonds, clubs 
}

package de.htwberlin.maven_aufgabe1.domain;

import java.util.ArrayList;
import java.util.List;

//
/**
 * The Class Game.
 */
public class Game {
	
	/** The deck open. */
	private Deck deckOpen = null;
	
	/** The deck closed. */
	private Deck deckClosed = new Deck();
	
	/** The players. */
	private List<Player> players;
	
	private int whosTurn;
	
	
	/**
	 * Instantiates a new game.
	 *
	 * @param playerList the player list
	 */
	public Game() {
		
		this.deckClosed = new Deck();
		this.deckOpen = new Deck();
		this.players = new ArrayList<Player>();
		this.whosTurn = 0;
		
	}
	
	/**
	 * Gets the open deck.
	 *
	 * @return the deck open
	 */
	public Deck getDeckOpen() {
		return deckOpen;
	}
	
	/**
	 * Gets the closed deck.
	 *
	 * @return the deck closed
	 */
	public Deck getDeckClosed() {
		return deckClosed;
	}
	
	
	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public List<Player> getPlayers() {
		return players;
	}

	/**
	 * Gets whosTurn
	 * @return
	 */
	public int getWhosTurn() {
		return whosTurn;
	}

	/**
	 * Sets whosTurn
	 * @param whosTurn
	 */
	public void setWhosTurn(int whosTurn) {
		this.whosTurn = whosTurn;
	}
	

	
}

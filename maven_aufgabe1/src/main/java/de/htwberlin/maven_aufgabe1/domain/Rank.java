package de.htwberlin.maven_aufgabe1.domain;
/**
 * Enumerations for the rank of the cards
 */
public enum Rank {

	six, seven, eight, nine, ten, jack, queen, king, ace
}

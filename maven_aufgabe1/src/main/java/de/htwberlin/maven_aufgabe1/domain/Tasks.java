package de.htwberlin.maven_aufgabe1.domain;

import java.util.List;


/**
 * The Interface Tasks for all operations of the game.
 */
public interface Tasks {
	
	/**
	 * Add a player to the game
	 * 
	 * @param game
	 * @param p the player
	 */
	public void addPlayer(Game game, Player p);
	
	/**
	 * Fills an empty deck in order of suites and ranks.
	 */
	public void generateDeck(Deck deck) ;
		
		/*
		 * {Suit[] suit = Suit.values();
		Rank[] rank = Rank.values();
		for(Suit s: suit) {
			for(Rank r: rank) {
				Card c = new Card(s,r);
				deck.getCards().add(c);
			}
		}}*/

	
	/**
	 * Shuffles the cards in deck.
	 *
	 * @param deck to be mixed
	 */
	public void mixCards(Deck deck);
	
	/**
	 * Hands out the cards from the deck to the players.
	 *
	 * @param deck which is covered
	 * @param players the players
	 */
	public void handOut(Deck deck, List<Player> players);
	

	/**
	 * Reveals the first card from the stack and put it on the openDeck.
	 *
	 * @param deck which is covered
	 * @param openDeck the deck for the played cards
	 */
	public void openFirstCard(Deck deck, Deck openDeck);
	
	/**
	 * Alert empty deck.
	 *
	 * @param deck which is covered
	 * @return the boolean
	 */
	public Boolean alertEmptyDeck(Deck deck);
	
	/**
	 * Turn the deck around and leave the last open card in the open deck.
	 *
	 * @param deck which is covered
	 * @param openDeck the deck for the played cards
	 */
	public void turnAroundDeck(Deck deck, Deck openDeck);
	
	/**
	 * Shows the open card.
	 *
	 * @param openDeck 
	 * @return the card which is open
	 */
	public Card checkPlacedCard(Deck openDeck);
	
	/**
	 * Check for fitting card in players deck.
	 *
	 * @param p the player
	 * @param openCard the top card
	 * @return the card which is to put on the open deck
	 */
	public Card checkMyCards(Player p, Card openCard);
	
	/**
	 * Player wishes a suit.
	 *
	 * @return Suit
	 */
	public Suit wish();
	
	/**
	 * Check for fitting card in case of wish.
	 *
	 * @param p the player
	 * @param suit the suit to check for
	 * @return the card which is to put on the open deck
	 */
	public Card checkMyCards(Player p, Suit suit);
	
	/**
	 * Player places a card.
	 *
	 * @param openDeck the open deck
	 * @param p the player
	 * @param myCard the my card
	 */
	public void placeCard(Deck openDeck, Player p, Card myCard);
	
	/**
	 * Player takes a card.
	 *
	 * @param deck the deck
	 * @param p the player
	 * @return card the top card from the deck
	 */
	public Card takeCard(Deck deck, Player p);
	
	/**
	 * Move on to the next player.
	 * @param game 
	 * @return int whosTurn
	 */
	public int nextPlayer(Game game);
	
	/**
	 * Player says mau.
	 *
	 * @param p the player
	 */
	public void sayMau(Player p);
	
	/**
	 * Player says mau mau.
	 *
	 * @param p the player
	 */
	public void sayMauMau(Player p);	
	
	/**
	 * Count the score of a player.
	 *
	 * @param p the player
	 */
	public void countScore(Player p);
	
	/**
	 * Show the end score of the players.
	 *
	 * @param players the players
	 */
	public void showScore(List<Player> players);
	
	/**
	 * Set up and start the game.
	 */
	public void startGame();
	
	
	
	
	
	

}

package de.htwberlin.maven_aufgabe1.domain;

import java.util.ArrayList;
import java.util.List;
//
/**
 * The Class Deck.
 */
public class Deck {

	/** The cards. */
	private List<Card> cards;
	
	/**
	 * Instantiates a new deck.
	 */
	public Deck() {
		cards = new ArrayList<Card>();
	}
	
	/**
	 * Gets the cards.
	 *
	 * @return the cards
	 */
	public List<Card> getCards() {
		return cards;
	}
		
}

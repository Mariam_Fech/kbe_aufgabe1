package de.htwberlin.maven_aufgabe1.domain;

//
/**
 * Defines the card with the rank and suit.
 */
public class Card {

	/** The suit. */
	private Suit suit;
	
	/** The rank. */
	private Rank rank;
	
	/**
	 * Instantiates a new card.
	 *
	 * @param suit the suit
	 * @param rank the rank
	 */
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	/**
	 * Gets the suit.
	 *
	 * @return the suit of the card
	 */
	public Suit getSuit() {
		return suit;
	}

	/**
	 * Getter method for rank
	 * @return rank of the card
	 */
	public Rank getRank() {
		return rank;
	}
	
	
	
}

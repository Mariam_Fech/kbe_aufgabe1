package de.htwberlin.maven_aufgabe1.domain;

//
/**
 * The Class Player.
 */
public class Player {

	/** The nickname. */
	private String nickname;
	
	/** The count cards. */
	private int countCards;
	
	/** The score. */
	private int score;
	
	/** The playerdeck. */
	private Deck pDeck;
	
	
	/**
	 * Instantiates a new player.
	 *
	 * @param nickname
	 */
	public Player(String nickname) {
		this.nickname = nickname;
		this.countCards = 0;
		this.score = 0;
		this.pDeck = new Deck();
	}
	
	/**
	 * Gets the nickname.
	 *
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * Sets the nickname.
	 *
	 * @param nickname the new nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	/**
	 * Gets the count cards.
	 *
	 * @return the count cards
	 */
	public int getCountCards() {
		return countCards;
	}
	
	/**
	 * Sets the count cards.
	 *
	 * @param countCards the new count cards
	 */
	public void setCountCards(int countCards) {
		this.countCards = countCards;
	}
	
	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Sets the score.
	 *
	 * @param score the new score
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * Gets the players deck.
	 *
	 * @return the players deck
	 */
	public Deck getpDeck() {
		return pDeck;
	}
	
	
}

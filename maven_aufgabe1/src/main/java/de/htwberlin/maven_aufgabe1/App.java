package de.htwberlin.maven_aufgabe1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final Logger LOGGER = LogManager.getLogger(App.class);
	
    public static void main( String[] args )
    {
    	LOGGER.debug("App gestartet");
        System.out.println( "Hello World!" );
        LOGGER.debug("App beendet");
    }
}
